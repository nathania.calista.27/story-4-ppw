from django.urls import path 

from story7 import views 

app_name = 'story7'

urlpatterns = [
    path('', views.home, name='home'), 
]
