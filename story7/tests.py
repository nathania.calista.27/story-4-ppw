from django.test import TestCase, Client 
from django.urls import resolve, reverse
from .views import * 

class TestStory7(TestCase):
    
    def test_home_function_exists(self):
        found = resolve(reverse('story7:home'))
        self.assertEqual(found.func, home)

    def test_home_template_used(self):
        response = self.client.get(reverse('story7:home'))
        html = response.content.decode('utf8')
        self.assertIn("My Profile", html)
        self.assertTemplateUsed(response, "story7.html")