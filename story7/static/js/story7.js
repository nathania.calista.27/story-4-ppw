
$(".acc-title").click(function() {

    $content  = $(this).parents(".accordion").find(".acc-content");  
    $title = $(this).parent(); 

    if ($content.hasClass("active")) {
        $content.removeClass("active"); 
        $title.css("background-color", ""); 
    } else {
        $content.removeClass("active"); 
        $content.addClass("active"); 
        $title.css("background-color", "#0081A2"); 
    }
}); 

$(".down").click(function() {
    $(".acc-content").removeClass("active");  
    $(".acc-header").css("background-color", ""); 
    var $el = $(this).parent().parent().parent(); 

    if ($el.not(":last-child")) {
        $el.next().after($el);  
    }
    
}); 

$(".up").click(function() {
    $(".acc-content").removeClass("active");  
    $(".acc-header").css("background-color", ""); 
    var $el = $(this).parent().parent().parent(); 

    if ($el.not(":first-child")) {
        $el.prev().before($el); 
    }
  
}); 

