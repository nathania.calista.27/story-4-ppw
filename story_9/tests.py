from django.test import TestCase
from django.urls import resolve, reverse  
from .views import * 
from django.contrib.auth.models import User

# Create your tests here.

class TestStory9(TestCase):
    
    # Home page 

    def test_home_url_exists(self):
        response = self.client.get(reverse('story_9:home'))
        self.assertEqual(response.status_code, 200)

    def test_home_template_used(self):
        response = self.client.get(reverse('story_9:home'))
        html = response.content.decode('utf8')
        self.assertIn("User Login", html)
        self.assertTemplateUsed(response, 'story_9/home.html')

    def test_home_views_func_used(self):
        found = resolve(reverse('story_9:home'))
        self.assertEqual(found.func, home)
    
    # login page 

    def test_login_url_exists(self):
        response = self.client.get(reverse('story_9:login'))
        self.assertEqual(response.status_code, 200)

    def test_login_template_used(self):
        response = self.client.get(reverse('story_9:login'))
        html = response.content.decode('utf8')
        self.assertIn("Login", html)
        self.assertTemplateUsed(response, 'story_9/login.html')


    # register page 
    def test_register_url_exists(self):
        response = self.client.get(reverse('story_9:register'))
        self.assertEqual(response.status_code, 200)
    
    def test_register_template_used(self):
        response = self.client.get(reverse('story_9:register'))
        html = response.content.decode('utf8')
        self.assertIn("Register", html)
        self.assertTemplateUsed(response, 'story_9/register.html')

    def test_user_exists_after_registration(self):
        datauser = {
            'username': 'test',
            'email' : 'test@mail.com',
            'password1': 'testingtesting123',
            'password2': 'testingtesting123'
        }
        
        response = self.client.post(reverse('story_9:register'), datauser)
        usertest = User.objects.get(username= "test")
        amount = User.objects.all().count()
        self.assertEqual(amount,1)
        self.assertEqual(usertest.username, 'test')
        self.assertEqual(usertest.email, 'test@mail.com')

    # logout page

    def test_logout_url_exists(self):
        response = self.client.get(reverse('story_9:logout'))
        self.assertEqual(response.status_code, 200)
    