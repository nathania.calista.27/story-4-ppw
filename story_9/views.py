from django.shortcuts import render, redirect
from .forms import UserRegistrationForm
from .models import Account
# Create your views here.

def home(request):
    return render(request, 'story_9/home.html')

def register(request):
    if request.method == "POST":
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            return redirect('story_9:login')
    else:
        form = UserRegistrationForm()
    return render(request, 'story_9/register.html', {'form':form})