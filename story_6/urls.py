from django.urls import path 

from . import views 

app_name = 'story_6'

urlpatterns = [
    path('', views.activities_home, name='home'),
    path('add_activity/', views.activities_add_activity, name='add_act'), 
    path('add_people/<int:id>/', views.activities_add_people, name='add_people')
]
