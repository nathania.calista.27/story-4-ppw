from django.db import models

# Create your models here.

class Activity(models.Model):
    name = models.CharField(max_length=100)
    date = models.DateField()

    def __str__(self):
        return self.name
    

class People(models.Model):
    name = models.CharField(max_length=100)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, related_name='participant')

    def __str__(self):
        return self.name
    