from django import forms
from django.forms import widgets, TextInput, DateInput
from .models import Activity, People

class ActivityForm(forms.ModelForm):
    
    class Meta:
        model = Activity
        fields = ['name', 'date']
        widgets = {
            'name' : TextInput(attrs={'placeholder' : 'Name', 'required' : True}), 
            'date' : DateInput(attrs={'placeholder' : 'YYYY-MM-DD', 'required' : True}), 
        }
            

class PeopleForm(forms.ModelForm):
    
    class Meta:
        model = People
        fields = ['name']
        widgets = {
            'name' : TextInput(attrs={'placeholder' : 'Name', 'required' : True}), 
        }
