from django.contrib import admin

# Register your models here.
from .models import Activity, People

admin.site.register(Activity)

admin.site.register(People)