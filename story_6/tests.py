from django.test import TestCase, Client

from django.http import HttpResponse, HttpRequest
from django.urls import resolve
from .models import Activity, People
from .views import * 


class TestActivity(TestCase):

### Test for url exists 

    def test_home_exists(self):
        response = Client().get('/story_6/')
        self.assertEquals(200, response.status_code)

    def test_add_exists(self):
        response = Client().get('/story_6/add_activity/')
        self.assertEquals(200, response.status_code)

### Test for add activity 

    def test_home_add_activity(self):
        Activity.objects.create(name="PPW", date="2020-10-01")
        found = resolve('/story_6/add_activity/')
        self.assertEquals(found.func, activities_add_activity)

### Test for template 

    def test_home_template(self):
        response = Client().get('/story_6/')
        self.assertTemplateUsed(response, 'activities.html')

    def test_add_template(self):
        response = Client().get('/story_6/add_activity/')
        self.assertTemplateUsed(response, 'add_activity.html')

### Test to create objects 

    def test_model_create_activity(self):
        Activity.objects.create(name="PPW", date="2020-10-01")
        amount = Activity.objects.all().count()
        self.assertEqual(amount, 1)

    def test_model_create_people(self):
        Activity.objects.create(name="PPW", date="2020-10-01")
        act1 = Activity.objects.get(id=1)
        
        People.objects.create(activity=act1, name="Pewe")
        amount = People.objects.all().count()
        self.assertEqual(amount, 1)

### Test for POST methods 

    def test_add_activity_post_url(self):
        response = Client().post('/story_6/add_activity/', data={'name': 'Eat', 'date' : '2020-10-01'})
        amount = Activity.objects.filter(name="Eat").count()
        self.assertEqual(amount, 1)

    def test_tambah_peserta_post_url(self):
        Activity.objects.create(name='PPW', date='2020-10-01')
        id1 = Activity.objects.get(id=1).id
        activity1 = Activity.objects.get(id=id1)
        People.objects.create(name='Pewe', activity=activity1)
        request = HttpRequest()
        response = activities_home(request)
        html_respone = response.content.decode('utf8')
        self.assertIn('Pewe', html_respone)

