from django.shortcuts import render, redirect

from .forms import ActivityForm, PeopleForm

from .models import Activity, People 

# Create your views here.


def activities_home(request):
    activity = Activity.objects.all()
    peopleform = PeopleForm(request.POST or None)
    return render(request, 'activities.html', {'activity' : activity, 'peopleform' : peopleform, })

def activities_add_activity(request):
    activityform = ActivityForm(request.POST or None)
    if (activityform.is_valid() and request.method == 'POST'):
        activityform.save()
        return redirect('story_6:home')

    return render(request, 'add_activity.html', {'activityform' : activityform, })

def activities_add_people(request, id=None):
    peopleform = PeopleForm(request.POST or None)
    activity = Activity.objects.get(id=id)
    if (peopleform.is_valid() and request.method == 'POST'):
        people_name = peopleform['name'].value()
        people = People(activity=activity, name=people_name)
        people.save()
        return redirect('story_6:home')
