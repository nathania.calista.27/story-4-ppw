from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'), 
    path('portfolio/', views.portfolio, name='portfolio'), 
    path('extra/', views.extra, name='extra'),
    path('story1/', views.story1, name='story1'),
    path('date/', views.date, name="date"), 
]
