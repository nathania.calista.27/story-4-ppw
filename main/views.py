from django.shortcuts import render
from datetime import datetime


def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def portfolio(request):
    return render(request, 'portfolio.html')

def extra(request):
    return render(request, 'extra.html')

def story1(request):
    return render(request, 'story1.html')

def date(request):
    today = str(datetime.now())
    return render (request, 'date.html', {"hello":today} )
