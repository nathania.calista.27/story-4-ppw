Story 4 ~ Story 9 

Nathania Calista
PPW C - PPW Gasal 2020/2021

Includes : 
* Story 4 - Django (Views + Template)
* Story 5 - Django (Models)
* Story 6 - TDD 
* Story 7 - JavaScript and JQuery 
* Story 8 - AJAX Dynamic Data Access
* Story 9 - Authentication, Cookie and Session 