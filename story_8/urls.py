from django.urls import path

from . import views 

app_name = 'story_8'

urlpatterns = [
    path('', views.home, name="home"), 
    path('<str:title>/', views.data, name="data"), 
   
]