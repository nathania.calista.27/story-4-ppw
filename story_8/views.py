from django.shortcuts import render
from django.http import JsonResponse  
import urllib, json

# Create your views here.

def home(request) :
    return render(request, 'story_8.html')

def data(request, title) :
    link = urllib.parse.quote("https://www.googleapis.com/books/v1/volumes?q="+ title,"/:?=")
    with urllib.request.urlopen(link) as url:
        global data 
        data = json.loads(url.read().decode())
    
    return JsonResponse(data, safe=False)

