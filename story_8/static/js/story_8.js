var noimage = "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/No_image_3x4.svg/200px-No_image_3x4.svg.png";

$("#keyword").keyup(function () {
    var keyword = $("#keyword").val();
    $.ajax({
        url : '/story_8/' + keyword, 
        success : function (result) {
            $("#result").empty();  

            var json = result.items; 
            for (i = 0; i < json.length; i++) {

                var volumeInfo = json[i].volumeInfo;
                var searchInfo = (json[i].searchInfo === undefined ? "" : json[i].searchInfo); 
                var obj_author = (volumeInfo.authors === undefined ? "-" : volumeInfo.authors);
                var obj_desc = (searchInfo.textSnippet === undefined ? "-" : searchInfo.textSnippet); 
                
                var html = ""; 
                html += '<div class="section">';
                html += '<div class="div-img"><img src=' + (volumeInfo.imageLinks === undefined ? noimage : volumeInfo.imageLinks.thumbnail) + '></div>';
                html += '<div class="content"><h4>' + volumeInfo.title + '</h4>';
                html += '<p> Author: ' + (obj_author.length > 3 ? obj_author.slice(0, 3) + " and  " + (obj_author.length - 3) + " other authors" : obj_author) + ' </p>';
                html += '<p> Description : </p> <p>' + truncateString(obj_desc, 300) + '</p>'; 
                html += '<a href=' + volumeInfo.infoLink + '> <b>Visit</b></span></a>' ; 
                html += '</div></div>' ;    
                $("#result").append(html); 
            }
        }

    }); 
}); 

function truncateString(str, length) {
    return str === undefined ? "-" : str.length > length ? str.substring(0, length - 3) + '...' : str
  }
  