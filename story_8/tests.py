from django.test import TestCase

# Create your tests here.

from django.test import TestCase, Client 
from django.urls import resolve, reverse
from .views import * 

class TestStory8(TestCase):
    
    def test_home_views_function_exists(self):
        found = resolve(reverse('story_8:home'))
        self.assertEqual(found.func, home)

    def test_home_template_used(self):
        response = self.client.get(reverse('story_8:home'))
        html = response.content.decode('utf8')
        self.assertIn("Book Search", html)
        self.assertTemplateUsed(response, "story_8.html")

    def test_data_views(self):
        response = self.client.get('/story_8/Fire%20Emblem/')
        self.assertIn("items", response.content.decode('utf-8'))
    