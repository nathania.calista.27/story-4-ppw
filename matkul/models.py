from django.db import models

# Create your models here.

class MataKuliah(models.Model):
    nama = models.CharField(max_length=100)
    dosen = models.CharField(max_length=100)
    sks = models.PositiveSmallIntegerField()
    desc = models.TextField(blank=True, null=True)
    
    class Semester(models.TextChoices):
        GASAL = 'Gasal',  ('Gasal')
        GENAP = 'Genap',  ('Genap')

    semester = models.CharField(max_length=15, choices=Semester.choices, default="Gasal")
    tahun = models.CharField(max_length=9)
    ruang = models.CharField(max_length=20)

    def __str__(self):
        return self.nama
