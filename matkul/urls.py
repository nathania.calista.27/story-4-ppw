from django.urls import path

from . import views

app_name = 'matkul'

urlpatterns = [
    path('home/', views.matkul_home, name='matkul_home'), 
    path('create/', views.matkul_create, name='matkul_create'),
    path('details/<int:my_id>/', views.matkul_details, name='matkul_details'),
    path('delete/<int:my_id>/', views.matkul_delete, name="matkul_delete"),
]