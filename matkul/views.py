from django.shortcuts import render, redirect

from .forms import MataKuliahForm

from .models import MataKuliah

# Create your views here.

def matkul_home(request):
    obj = MataKuliah.objects.all()
    context = {
        'obj' : obj
    }
    return render(request, 'matkul_home.html', context)

def matkul_create(request):
    form = MataKuliahForm(request.POST or None)
    if form.is_valid():
        form.save() 
        return redirect('matkul:matkul_home')

    context = {
        'form' : form 
    }
    
    return render(request, 'matkul_create.html', context)

def matkul_details(request, my_id):
    obj = MataKuliah.objects.get(id=my_id)
    context = {
        'obj' : obj
    }
    return render(request, 'matkul_details.html', context)

def matkul_delete(request, my_id):
    obj = MataKuliah.objects.get(id=my_id)
    obj.delete()
    return redirect('matkul:matkul_home')